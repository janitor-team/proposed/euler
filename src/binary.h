/*
 *	Euler - a numerical lab
 *
 *	platform : neutral
 *
 *	file : binary.h -- file access
 */

#ifndef _BINARY_H_
#define _BINARY_H_

#include "stack.h"

void mopen (header *hd);
void mclose (header *hd);
void meof (header *hd);

void mwrite (header *hd);

void mputchar (header *hd);
void mputword (header *hd);
void mputlongword (header *hd);

#if 0
void mputuchar (header *hd);
void mputuword (header *hd);
void mputulongword (header *hd);

void mgetuchar (header *hd);
void mgetuchar1 (header *hd);
void mgetuword (header *hd);
void mgetuword1 (header *hd);
void mgetulongword (header *hd);
void mgetulongword1 (header *hd);
#endif

void mgetchar (header *hd);
void mgetchar1 (header *hd);
void mgetstring (header *hd);
void mgetword (header *hd);
void mgetword1 (header *hd);
void mgetlongword (header *hd);
void mgetlongword1 (header *hd);
void mgetvector (header *hd);


#endif
