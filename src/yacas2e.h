#ifndef _YACAS_H_
#define _YACAS_H_

#include "stack.h"

#define YACAS 1

void init_yacas ();
void exit_yacas ();
void interrupt_yacas();
char * call_yacas (char *);

void myacas (header *hd);
void myacaseval (header *hd);
void myacasclear (header *hd);

#endif
