/*
 *	Euler - a numerical lab
 *
 *	file : udf.h -- user defined function management
 */

#ifndef _UDF_H_
#define _UDF_H_

#include "stack.h"

extern int udfon;

void mdo (header *hd);
void make_xors (void);

/*
 *	commands
 */
void get_udf (void);
void do_type (void);
void do_trace(void);

void do_global (void);
void do_useglobal (void);

void do_if (void);
void do_else (void);
void do_elseif (void);
void do_endif (void);

void do_repeat (void);
void do_loop (void);
void do_for (void);
void do_break (void);
void do_end (void);

void do_return (void);

/*
 *	builtin
 */
void mindex (header *hd);

header *searchudf (char *name);

void interpret_udf (header *var, header *args, int nargs, int sp);
void trace_udfline (char *next);

#endif
