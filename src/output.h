/*
 *	Euler - a numerical lab
 *
 *	file : output.h -- output and formating functions
 */
 
#ifndef _OUTPUT_H_
#define _OUTPUT_H_

#include "stack.h"

/*
 *	format setup functions
 */
void mformat (header *hd);
void mgformat (header *hd);
void meformat (header *hd);
void mfformat (header *hd);
void miformat (header *hd);
void mfracformat (header *hd);

/*
 *	generic output (real, complex, matrix, ...)
 */
void give_out (header *hd);

/*
 *	basic output
 */
void output (char *s);
void output1 (char *s, ...);
void output1hold (int f, char *s, ...);
void print_error (char *p);
#define output2(form,s,t) output1(form,s,t)

#define wrong_arg() { error=26; output("Wrong argument\n"); return; }
#define wrong_arg_in(x) { error=26; output1("Wrong arguments for %s\n",x); return; }
#define test_error(x) { if (error) { output1("Error in %s\n",x); } }
#define varnotfound(x) { output1("Variable not found in %s\n",x); }
#define outofram() { output("Out of Memory!\n"); error=120; return; }

#endif
