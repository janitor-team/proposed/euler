/*
 *	Euler - a numerical lab
 *
 *	file : polynom.h -- polynomial maths
 */
 
#ifndef _POLYNOM_H_
#define _POLYNOM_H_

#include "stack.h"

void polyval (header *hd);
void polyadd (header *hd);
void polymult (header *hd);
void polydiv (header *hd);
void dd (header *hd);
void ddval (header *hd);
void polydd (header *hd);
void polyzeros (header *hd);
void polytrunc (header *hd);
void mzeros (header *hd);
void mzeros1 (header *hd);

#endif
