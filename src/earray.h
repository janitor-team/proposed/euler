/*
 *	Euler - a numerical lab
 *
 *	platform : neutral
 *
 *	file : earray.h -- notebook line array
 */

#ifndef E_ARRAY_H
#define E_ARRAY_H


#include <glib.h>


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define	E_OUTPUT	0
#define E_PROMPT	1
#define E_UDF		2
#define E_COMMENT	3

typedef struct earray earray;

earray *	e_new();
void		e_free(earray *a);

int			e_load(earray *a, char *filename);
int			e_save(earray *a, char *filename);

int			e_get_length(earray *a);

void		e_clear(earray *a);
int			e_append(earray *a, char *text, int type);
int			e_insert(earray *a, int index, char *text, int type);
void		e_remove(earray *a, int index);

void		e_set_type(earray *a, int index, int type);
int			e_get_type(earray *a, int index);
void		e_set_text(earray *a, int index, char *text);
char *		e_get_text(earray *a, int index);
void		e_append_char(earray *a, int index, char c);
void		e_append_text(earray *a, int index, char *text);
void		e_insert_char(earray *a, int index, int pos, char c);
void		e_insert_text(earray *a, int index, int pos, char *text);
void		e_remove_text(earray *a, int index, int pos, int len);

int			e_get_text_length(earray *a, int index);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
