#include <stdio.h>
#include <ctype.h>
#include <string.h>

#include "sysdep.h"
#include "output.h"
#include "graphics.h"
#include "mainloop.h"

char fktext [10][64];

extern int outputing;
int nojump;


static void prompt (void)
{	if (!outputing) gprint("\n>");
	if (!udf) output(">");
	else output("$");
}


static char *editline;

/* For external use in notebooks */
void set_editline (char *p)
{   strcpy(editline,p);
}

void edit (char *s)
{	int scan;
	char ch;
	
	editline=s;
	s[0]=0;
	prompt();
	edit_on();
	while (1)
	{	ch=wait_key(&scan);
		if (scan==switch_screen) show_graphics();
		if (scan==enter) break;
	}
	if (outfile) fprintf(outfile,"%s",s);
    edit_off();
	output("\n");
}
