/*
 *	Euler - a numerical lab
 *
 *	platform : neutral
 *
 *	file : express.h -- expression parsing
 */

#ifndef _EXPRESS_H_
#define _EXPRESS_H_

#include "stack.h"

extern char *next;

header *scan(void);
header *scan_value(void);
void scan_space (void);
void scan_namemax (char *name, int lmax);
void scan_name (char *name);

#endif
