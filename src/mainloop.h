#ifndef _MAINLOOP_H_
#define _MAINLOOP_H_

#include <stdio.h>

extern int udf;
extern int outputing;
extern int errorout;
extern int trace;
extern int quit;

extern double epsilon,changedepsilon;
extern int actargn,actsp;
extern int stringon;
extern int udfon;

extern int searchglobal;

extern char *udfline;
extern char input_line[1024];

extern FILE *infile, *outfile;

int command (void);
void main_loop (int argc, char *argv[]);

#endif
