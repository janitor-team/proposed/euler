/*
 *	Euler - a numerical lab
 *
 *	platform : neutral
 *
 *	file : fft.h -- fast fourier transform
 */

#ifndef _FFT_H_
#define _FFT_H_

#include "stack.h"

void mfft (header *hd);
void mifft (header *hd);

#endif
