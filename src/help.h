/*
 *	Euler - a numerical lab
 *
 *	platform : neutral
 *
 *	file : help.h -- help management
 */

#ifndef _HELP_H_
#define _HELP_H_

void externhelp (char *text);
void loadhelp (char *name);
void unloadhelp ();

#endif
