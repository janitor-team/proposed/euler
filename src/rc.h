/*
 *	Euler - a numerical lab
 *
 *	file : rc.h -- resource file handling
 */

#ifndef _RC_H_
#define _RC_H_

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define E_SAVEATEXIT_DEFAULT	1

#define E_TWIDTH_DEFAULT		80
#define E_THEIGHT_DEFAULT		24
#define E_TFONT_DEFAULT			"7x14"
//#define E_TFONT_DEFAULT  "-*-courier-medium-r-*-*-*-120-*-*-*-*-*-*"
#define E_PFONT_DEFAULT			"Terminal 10"
	
#define E_GWIDTH_DEFAULT		500
#define E_GHEIGHT_DEFAULT		500

#define E_ESTACK_DEFAULT		8*1024l
#define E_GSTACK_DEFAULT		4*1024l
#define E_GLINES_DEFAULT		40

#define E_BROWSER_DEFAULT		"netscape"

#define MAX_COLORS				16


typedef struct _EulerPrefs {
	int		saveatexit;
	int		estack;
	int		twidth;
	int		theight;
	int		gwidth;
	int		gheight;
	int		gstack;
	int		glines;
	short	colors[3][MAX_COLORS];
	char	browser[256];
	char	tfont[256];
	char	gfont[256];
	char	pfont[256];
} EulerPrefs;

EulerPrefs eulerrc_init(void);
void eulerrc_save(EulerPrefs *prefs);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __RC_H__ */
