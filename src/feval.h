/*
 *	Euler - a numerical lab
 *
 *	platform : neutral
 *
 *	file : feval.h -- math solvers
 */

#ifndef _FEVAL_H_
#define _FEVAL_H_

#include "stack.h"

void mbrent (header *hd);
void mnelder (header *hd);
void mrunge1 (header *hd);
void mrunge2 (header *hd);

#endif
