/*
 *	Euler - a numerical lab
 *
 *	platform : neutral
 *
 *	file : meta.h -- portable independant graphic system
 */

#ifndef _META_H_
#define _META_H_

#include <glib.h>
#include "stack.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#ifndef MAX_COLORS
#define MAX_COLORS	16
#endif
/*
 * device structure definition
 */
typedef void (*scalefn)(void *data, double s);
typedef void (*clearfn)(void *data);
typedef void (*clipfn)(void *data, double c, double r, double c1, double r1);
typedef void (*linefn)(void *data, double c, double r, double c1, double r1, int color, int st, int width);
typedef void (*markerfn)(void *data, double c, double r, int color, int st);
typedef void (*barfn)(void *data, double c, double r, double c1, double r1, double hue, int color, int connect);
typedef void (*bar1fn)(void *data, double c, double r, double c1, double r1, int color, int connect);
typedef void (*fillfn)(void *data, double c[], int st, int n, int connect[]);
typedef void (*fillhfn)(void *data, double c[], double hue, int color, int connect);
typedef void (*textfn)(void *data, double c, double r, char *text, int color, int alignment);
typedef	void (*vtextfn)(void *data, double c, double r, char *text, int color, int alignment);
typedef void (*vutextfn)(void *data, double c, double r, char *text, int color, int alignment);
typedef void (*beginplayfn)(void *data);
typedef void (*endplayfn)(void *data);
//typedef void (*colorallocatefn)(short colors[3][MAX_COLORS]);

typedef struct metadevice {
	void *		data;			/* private data pointer */
	beginplayfn	begin;
	endplayfn	end;
	clearfn		clear;
	clipfn		clip;
	linefn		line;
	markerfn	marker;
	barfn		bar;
	bar1fn		bar1;
	fillfn		fill;
	fillhfn		fillh;
	textfn		text;
	vtextfn		vtext;
	vutextfn	vutext;
	scalefn		scale;
} metadevice;

/*
 *	metafile programming api
 */

int				openmeta(int size, metadevice *device);
void			closemeta();

void			playmeta();

metadevice *	setmetadevice(metadevice *d);
metadevice *	getmetadevice();

void			setmetawidth(int width);
void			setmetaheight(int height);
int 			getmetawidth();
int				getmetaheight();

void			setmetalines(int lines);
int				getmetalines();
void			setmetacharwidth(int width);
void			setmetacharheight(int height);
int 			getmetacharwidth();
int				getmetacharheight();

void			setmetacolors(short colors[3][MAX_COLORS]);
short			getmetacolor(int rgb, int index);


/*
 *	specific device output
 */

int dump_meta (char *filename);

void pswindow (double w, double h);

/*
 *	basic drawing commands
 */

void gclear (void);
void gclip(double c, double r, double c1, double r1);
void gline (double c, double r, double c1, double r1, int color, int st, int width);
void gtext (double c, double r, char *text, int color, int centered);
void gvtext (double c, double r, char *text, int color, int centered);
void gvutext (double c, double r, char *text, int color, int centered);
void gmarker (double c, double r, int color, int st);
void gfill (double c[], int st, int n, int connect[]);
void gfillh (double c[], double hue, int color, int connect);
void gbar (double c, double r, double c1, double r1, double hue, int color, int connect);
void gbar1 (double c, double r, double c1, double r1, int color, int connect);
void gscale (double s);


/*
 *	frame support
 */

void mbeginframes(header *hd);
void mendframes(header *h);

void mframes(header *hd);
void mplayframes(header *hd);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
