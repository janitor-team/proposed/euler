
%define ver      1.61.0
%define rel      1
%define prefix   /usr

Summary: A program for doing mathematics
Name: euler
Version: %ver
Release: %rel
Copyright: GPL
Group: Applications/Science
Source: http://prdownloads.sourceforge.net/euler/euler-%{ver}.tar.gz
Packager: Eric Bouchar� <bouchare.eric@wanadoo.fr>
%description
Euler is a program for quickly and interactively computing with real and
complex numbers and matrices, or with intervals. It can draw your functions in
two and three dimensions.

%build
make

%install
make install

%clean
make uninstall

%files
%{prefix}/bin/euler
%{prefix}/share/euler/help.txt
%dir %{prefix}/share/euler/progs/*
%dir %{prefix}/share/doc/euler/*
%dir %{prefix}/share/doc/euler/images/*
%dir %{prefix}/share/doc/euler/french/*
%dir %{prefix}/share/doc/euler/german/*
%dir %{prefix}/share/doc/euler/german/images/*
%dir %{prefix}/share/doc/euler/reference/*
