%------------------------------------
% File:           westerbench.en
% M. Wester's CAS benchmark and Yacas 
%------------------------------------
% In his 1994 paper Review of CAS mathematical capabilities, Michael Wester has put forward 
% 123 problems that a reasonable computer algebra system should be able to solve and tested 
% the then current versions of various commercial CAS on this list. 
% Below is the list of Wester's problems with the corresponding Yacas code. 
% "OK"  means a satisfactory solution, 
% "BUG" means that Yacas gives a wrong solution or breaks down, 
% "NO"  means that the relevant functionality is not yet implemented. 
% Yacas version: 1.0.57 
%
% Eine Auswahl von Tests aus dem Wester Benchmark
%      http://www.math.unm.edu/~wester/cas_review.html
% in M. Wester et. al.: Computer Algebra Systems: A Practical Guide. Wiley, 1999.
%      http://www.math.unm.edu/~wester/cas/book/contents.html
% Tests fuer Yacas zusammengestellt von A. Pinkus/YaCAS.
% .. Mit kleinen Korrekturen und �nderungen als EULER-Notebook aufgeschrieben 
%    von W. Lindner, 8/2005
% .. Manche Berechnungen werden hier alternativ durchgef�hrt.
% .. Neben dem Test mit Verify zeigen wir zusaetzlich das YaCAS-Ergebnis.
%
%1. Berechnung von 50! 
%   (OK) Factorize 50!
> yacas("50! ") 
> yacas("Verify(25!, 15511210043330985984000000)")
> yacas("Verify(50!, (26***50)*25!)")
%
%2. Die Primfaktorzerlegung von 6!
%(OK)    Prime factors of 6!
> yacas("ans:=Factors(6!)") 
% Out> {{2,4},{5,1},{3,2}} 
%Die Liste ist folgenderma�en zu interpretieren:
>> FW(ans)
% 4        2
%2  * 5 * 3 
%Out> True 
%
%3. Berechnung von 1/2 + ... + 1/10 :
%   (OK)    Calculate 1/2+...+1/10=4861/2520.
> yacas("  Sum(i,2,10,1/i) ")
> yacas("  Verify(Sum(n,2,10,1/n) , 4861/2520) ")
% Out> 4861/2520 
%
%4. Berechnung eines N�herungswertes f�r e^(Pi*sqrt(163)) auf 50 Stellen :
%   (OK)    Evaluate e^(Pi*Sqrt(163)) to 50 decimal digits
> yacas(" Precision(50) ")
> yacas(" N(Exp(Pi*Sqrt(163))) ")
> yacas("Verify(N(1000000000000*(-262537412640768744 +  Exp(Pi*Sqrt(163))), 50)> -0.75, True)")
% Out> 262537412640768743.99999999999925007259719818568885219682604177332393 
%
%5. Berechnung der Dezimaldarstellung von 1/7 :
%   (OK)    Obtain period of decimal fraction 1/7=0.(142857).
> yacas(" Decimal(1/7) ")
% Out> {0,{1,4,2,8,5,7}} 
% Das Ergebnis ist folgenderma�en zu lesen: 0.142857....
% The result is to interpret as follows:
%
%6. Berechnung von Pi als Kettenbruch :
%   (OK)    Continued fraction of 3.1415926535. 
> yacas("Verify([Local(p,r);p:=GetPrecision();Precision(12);r:=ContFracList(3.1415926535, 6);Precision(p);r;],{3,7,15,1,292,1}) ")
>> ContFrac(Pi())
> yacas(" pi:=N(Pi,20)  ")
%
%                 1             
%3 + ---------------------------
%                   1           
%    7 + -----------------------
%                     1         
%        15 + ------------------
%                       1       
%             1 + --------------
%                          1    
%                 292 + --------
%                       1 + rest
%
%Out> True 
%
%7. Vereinfachung von sqrt(2*sqrt(3)+4) :
%  (OK) Sqrt(2*Sqrt(3)+4)=1+Sqrt(3). 
> yacas("Verify(RadSimp(Sqrt(2*Sqrt(3)+4)), 1+Sqrt(3)) ")
> yacas("  RadSimp(Sqrt(2*Sqrt(3)+4))  ")
% Out> 1+Sqrt(3) 
%
%8. Vereinfachung von sqrt(14+3*sqrt(3+2*sqrt(5-12*sqrt(3-2*sqrt(2))))) :
%  (OK) Sqrt(14+3*Sqrt(3+2*Sqrt(5-12*Sqrt(3-2*Sqrt(2)))))=3+Sqrt(2).
%  .. please wait .. I'm working .. 
> yacas("Verify(RadSimp(Sqrt(14+3*Sqrt(3+2*Sqrt(5-12*Sqrt(3-2*Sqrt(2)))))), 3+Sqrt(2)) ")
> yacas(" RadSimp(Sqrt(14+3*Sqrt(3+2*Sqrt(5-12*Sqrt(3-2*Sqrt(2))))))  ")
% Out> 3+Sqrt(2) 
%
%9. Vereinfachung von 2*infinity-3. :
%   (OK) 2*Infinity-3=Infinity. 
> yacas("Verify(2*Infinity-3, Infinity) ")
> yacas(" 2*Infinity-3  ")
% Out> Infinity 
%
%(NO) Standard deviation of the sample (1, 2, 3, 4, 5). 
%(NO) Hypothesis testing with t-distribution. 
%(NO) Hypothesis testing with normal distribution 
%     (M. Wester probably meant the chi^2 distribution). 
%
%10. Vereinfachung von (x^2-4)/(x^2+4x+4) :
%(OK) (x^2-4)/(x^2+4*x+4)=(x-2)/(x+2). 
> yacas("Verify(GcdReduce((x^2-4)/(x^2+4*x+4),x),  (x-2)/(x+2)) ")
>>  GcdReduce((x^2-4)/(x^2+4*x+4),x)
% 
%-2 + x
%------
%2 + x 
%
%Out> True 
%
%(NO) (Exp(x)-1)/(Exp(x/2)+1)=Exp(x/2)-1. 
%
%(OK) Expand (1+x)^20, take derivative and factorize. 
> yacas("Factor(D(x) Expand((1+x)^20)) ")
%
%11. Ausmultiplizieren, Ableiten und Faktorisieren von (x+1)^5 :
%    (OK) Expand (1+x)^20, take derivative and factorize.
> yacas("  ans:=Factors(D(x)Expand((x+1)^5)) ")
>> FW(ans)
% 
%         4    
%( 1 + x )  * 5
%
%Out> True 
%
%12. (BUG/NO) Factorize x^100-1. 
> yacas("Factor(x^100-1) ")
%(returns the same expression unfactorized) 
%
%(NO) Factorize x^4-3*x^2+1 in the field of rational numbers extended by roots of x^2-x-1. 
%(NO) Factorize x^4-3*x^2+1 mod 5. 
%
%13. (BUG) Partial fraction decomposition of (x^2+2*x+3)/(x^3+4*x^2+5*x+2). 
>> Apart((x^2+2*x+3)/(x^3+4*x^2+5*x+2), x)
%      (does not obtain full partial fraction representation 
%      for higher-degree polynomials, e.g. p(x)/(x+a)^n ) 
%
%(NO) Assuming x>=y, y>=z, z>=x, deduce x=z. 
%(NO) Assuming x>y, y>0, deduce 2*x^2>2*y^2. 
%(NO) Solve the inequality Abs(x-1)>2. 
%(NO) Solve the inequality (x-1)*...*(x-5)<0. 
%
%(NO) Cos(3*x)/Cos(x)=Cos(x)^2-3*Sin(x)^2 or similar equivalent combination. 
%(NO) Cos(3*x)/Cos(x)=2*Cos(2*x)-1. 
%14. (OK) Define rewrite rules to match Cos(3*x)/Cos(x)=Cos(x)^2-3*Sin(x)^2. 
> yacas("Cos(3*_x)/Cos(_x) <-- Cos(x)^2-3*Sin(x)^2 ")
> yacas("Simplify(Cos(3*x)/Cos(x)) ")
% 
%15. Vereinfachung von sqrt(997) - (997^3)^(1/6) :
%(OK) Sqrt(997)-997^3^(1/6)=0 
> yacas(" Verify(RadSimp(Sqrt(997)-(997^3)^(1/6)), 0) ")
> yacas("  RadSimp(Sqrt(997)-997^3^(1/6))  ")
% Out> 0 
%
%16. Vereinfachung von sqrt(999983) - (999983^3)^(1/6) :
%   (OK) Sqrt(99983)-99983^3^(1/6)=0 
> yacas(" Verify(RadSimp(Sqrt(99983)-(99983^3)^(1/6)) , 0) ")
> yacas(" RadSimp(Sqrt(999983)-999983^3^(1/6))  ")
% Out> 0 
%
%17. Erkennen, dass (2^(1/3)+4^(1/3))^3-6*(2^(1/3)+4^(1/3)) - 6 gleich 0 ist :
%(OK) (2^(1/3)+4^(1/3))^2-6*(2^(1/3)+4^(1/3))-6=0 
> yacas(" Verify(RadSimp((2^(1/3)+4^(1/3))^3-6*(2^(1/3)+ 4^(1/3))-6), 0) ")
> yacas(" RadSimp((2^(1/3)+4^(1/3))^3-6*(2^(1/3)+4^(1/3))-6)  ")
% Out> 0 
%
%18. (NO) Ln(Tan(x/2+Pi/4))-ArcSinh(Tan(x))=0 
> yacas(" Ln(Tan(x/2+Pi/4))-ArcSinh(Tan(x)) ")
%
%(NO) Numerically, the expression Ln(Tan(x/2+Pi/4))-ArcSinh(Tan(x))=0 and its derivative at x=0 are zero. 
> yacas(" D(x)(Ln(Tan(x/2+Pi/4))-ArcSinh(Tan(x))) ")
% 
%(NO) Ln((2*Sqrt(r)+1)/Sqrt(4*r+4*Sqrt(r)+1))=0. 
%(NO) (4*r+4*Sqrt(r)+1)^(Sqrt(r)/(2*Sqrt(r)+1))*(2*Sqrt(r)+1)^(2*Sqrt(r)+1)^(-1)-2*Sqrt(r)-1=0, assuming r>0. 
%
%19. (OK) Obtain real and imaginary parts of Ln(3+4*I). 
> yacas(" Verify(  Hold({ {x}, {Re(x), Im(x)}}) @ Ln(3+4*I) , {Ln(5),ArcTan(4/3)}) ")
> yacas(" Hold({ {x}, {Re(x), Im(x)}}) @ Ln(3+4*I) ")
% 
%20. (BUG) Obtain real and imaginary parts of Tan(x+I*y). 
> yacas(" Hold({ {x}, {Re(x), Im(x)}}) @ Tan(x+I*y) ")
%
%21. (BUG) Simplify Ln(Exp(z)) to z for -Pi<Im(z)<=Pi. 
> yacas(" Verify(Simplify(Ln(Exp(z))), z) ")
> yacas(" Simplify(Ln(Exp(z))) ")
%(no conditions on z are used) 
%
%(NO) Assuming Re(x)>0, Re(y)>0, deduce x^(1/n)*y^(1/n)-(x*y)^(1/n)=0. 
%(NO) Transform equations, (x==2)/2+(1==1)=>x/2+1==2. 
%(BUG) Solve Exp(x)=1 and get all solutions. Verify(Solve(Exp(x)==1,x), {x==0});
%
%22. Umwandlung von log e^z in z :
> yacas(" Simplify(Ln(Exp(z)))  ")
% Out> z 
%
%23. Invertieren der 2x2 Matrix [[a,b],[1,ab]] :
%    (the new routine Solve cannot do this yet) 
%(OK) Invert a 2x2 symbolic matrix. 
> yacas(" Verify(Simplify(Inverse({{a,b},{1,a*b}})), {{a/(a^2-1), -1/(a^2-1)}, {-1/(b*(a^2-1)), a/(b*(a^2-1))}}) ")
>> Simplify(Inverse( {{a,b},{1,a*b}} ) )
%
>>  A:={{a,b},{1,a*b}}
>> ans:=Inverse(A)
% Out> {{(a*b)/(b*a^2-b),(-b)/(b*a^2-b)},{-1/(b*a^2-b),a/(b*a^2-b)}} 
>> Simplify(ans)
% {1/(a+ -1/a),1/(1-a^2)}
% {1/(b-b*a^2),1/(b*a-b/a)}
% Out> True 
%
%24. (BUG) Compute the determinant of the 4x4 Vandermonde matrix. 
> yacas(" Factor(Determinant(VandermondeMatrix ({a,b,c,d}))) ")
%(this does not factor correctly) 
%
%25. Berechnung der Eigenwerte der Matrix [[5, -3, -7],[-2, 1, 2],[ 2, -3, -4]] :
%(OK) Find eigenvalues of a 3x3 integer matrix. 
> yacas(" Verify(EigenValues({{5,-3,-7},{-2,1,2}, {2,-3,-4}}) , {1,-2,3}) ")
>>  A:={{5,-3,-7},{-2,1,2},{2,-3,-4}}
> yacas(" EigenValues(A)  ")
% Out> {1,3,-2} 
%
%26. Limes von (1-cos x)/x^2 f�r x gegen Null :
%(OK) Verify some standard limits found by L'Hopital's rule: 
> yacas(" Verify(Limit(x,Infinity) (1+1/x)^x, Exp(1)) ")
> yacas(" Verify(Limit(x,0) (1-Cos(x))/x^2, 1/2) ")
> yacas(" Limit(x,0)(1-Cos(x))/x^2  ")
% Out> 1/2 
%
%27. Ableitung von |x| :
%(OK) D(x)Abs(x) 
> yacas(" Verify(D(x) Abs(x), Sign(x)) ")
> yacas(" D(x)Abs(x)  ")
% Out> Sign(x) 
%
%28. Stammfunktion von |x| :
%(OK) (Integrate(x)Abs(x))=Abs(x)*x/2 
> yacasclear  .. notwendig wg obiger Definition von A
> yacas(" Verify(Simplify(Integrate(x) Abs(x)),  Abs(x)*x/2) ")
> yacas(" Simplify(Integrate(x) Abs(x)) ")
> yacas(" Integrate(x) Abs(x) ")
> yacas(" AntiDeriv(Abs(x),x)  ")
% Out> (Abs(x)*x)/2 
%
%29. Ableitung von |x| (st�ckweise definiert) :
%(OK) Compute derivative of Abs(x), piecewise defined.
> yacas("  Verify(D(x)if(x<0) (-x) else x,  Simplify(if(x<0) -1 else 1)) ")
>> D(x) if(x<0) (-x) else x
% Out>  if(x<0) -1 else 1 
%
%30. Stammfunktion von |x| (st�ckweise definiert) :
%(OK) Integrate Abs(x), piecewise defined. 
> yacas(" Verify(Simplify(Integrate(x)  if(x<0) (-x) else x),  Simplify(if(x<0) (-x^2/2) else x^2/2)) ")
> yacas(" Integrate(x)  if(x<0) (-x) else x ")
> yacas(" AntiDeriv(if(x<0)(-x)else x,x) ")
% Out> if(x<0)(-x^2/2)else x^2/2 
%
%31. Die ersten Summanden der Taylorentwicklung von
%    1/sqrt(1-v^2/c^2) an der Stelle v=0 :
%(OK) Taylor series of 1/Sqrt(1-v^2/c^2) at v=0. 
> yacas(" S := Taylor(v,0,4) 1/Sqrt(1-v^2/c^2) ")
> yacas(" TestYacas(S, 1+v^2/(2*c^2)+3/8*v^4/c^4) ")
%
%Note: The result of Taylor is not in convenient form but is equivalent. 
%
> yacas(" ans:=Taylor(v,0,4)Sqrt(1/(1-v^2/c^2))  ")
>> Simplify(ans)
% 
%       2          4
%      v      3 * v 
%1 + ------ + ------
%         2        4
%    2 * c    8 * c 
%
%Out> True 
%
%32. Der Kehrwert des Quadrates der obigen L�sung :
%(OK) Compute the Taylor expansion of the inverse square of the above. 
%    .. wait ..
> yacas(" TestYacas(Taylor(v,0,4) 1/S^2, 1-v^2/c^2) ")
%
%Note: The result of Taylor is not in convenient form but is equivalent. 
%
> yacas(" ans:=Taylor(v,0,4)(1/ans)^2  ")
>> Simplify(ans)
% 
%     2
%    v 
%1 - --
%     2
%    c 
%
%Out> True 
%
%33. Berechnung der Taylorentwicklung von tan(x) an der Stelle x=0
%    durch Dividieren der Entwicklungen von sin(x) und cos(x) :
%(OK) (Taylor expansion of Sin(x))/(Taylor expansion of Cos(x)) = (Taylor expansion of Tan(x)). 
> yacas(" TestYacas(Taylor(x,0,5)(Taylor(x,0,5)Sin(x)) / (Taylor(x,0,5)Cos(x)), Taylor(x,0,5)Tan(x)) ")
%
> yacas(" Taylor(x,0,5)(Taylor(x,0,5)Sin(x)) / (Taylor(x,0,5)Cos(x)) ")
> yacas(" Taylor(x,0,5)Tan(x) ")
%
> yacas(" ans1:=Taylor(x,0,5)Sin(x)/Cos(x)  ")
%
%     3        5
%    x    2 * x 
%x + -- + ------
%    3      15  
%
> yacas(" ans2:=Taylor(x,0,5)Tan(x)  ")
%
%     3        5
%    x    2 * x 
%x + -- + ------
%    3      15  
%
> yacas(" ans1-ans2  ")
% Out> 0 
%
%34. (BUG) Taylor expansion of Ln(x)^a*Exp(-b*x) at x=1.
> yacas(" Taylor(x,1,3)(Ln(x))^a*Exp(-b*x) ")
% (bugs in Deriv manipulation) 
%
%35. (BUG) Taylor expansion of Ln(Sin(x)/x) at x=0.
> .. yacas(" Taylor(x,0,5) Ln(Sin(x)/x) ")
%(never stops) 
%
%(NO) Compute n-th term of the Taylor series of Ln(Sin(x)/x) at x=0. 
%(NO) Compute n-th term of the Taylor series of Exp(-x)*Sin(x) at x=0. 
%
%36. (OK) Solve x=Sin(y)+Cos(y) for y as Taylor series in x at x=1. 
> yacas(" TestYacas(InverseTaylor(y,0,4) Sin(y)+Cos(y), (y-1)+(y-1)^2/2+2*(y-1)^3/3+(y-1)^4) ")
> yacas(" InverseTaylor(y,0,4) Sin(y)+Cos(y) ")
%
%Note that InverseTaylor does not give the series in terms of x but in terms of y 
%which is semantically wrong. But other CAS do the same. 
%
%37. Direkte Berechnung der Legendre Polynome :
%(OK) Compute Legendre polynomials directly from Rodrigues's formula, 
%     P[n]=1/(2*n)!! *(Deriv(x,n)(x^2-1)^n). 
> yacas(" P(n,x) := Simplify( 1/(2*n)!! *  Deriv(x,n) (x^2-1)^n ) ")
> yacas(" TestYacas(P(4,x), (35*x^4)/8+(-15*x^2)/4+3/8) ")
> yacas(" P(4,x) ")
%
> yacas(" 10#Legendre(0,_x)<--1 ")
> yacas(" 20#Legendre(n_IsInteger,_x)<--[Local(result);result:=[Local(x);Expand(1/(2^n*n!)*Deriv(x,n)Expand((x^2-1)^n,x));];Eval(result);]  ")
> yacas(" Table(Legendre(i,x), i,0,4,1)  ")
%
%1
%
%x
%
%          2
%-1 + 3 * x 
%-----------
%     2     
%...
%
%38. Rekursive Berechnung der Legendre Polynome :
%(OK) Compute Legendre polynomials P[n] recursively. 
> yacas(" Verify(OrthoP(4,x) , 3/8+((35*x^2)/8-15/4)*x^2) ")
> yacas(" OrthoP(4,x) ")
%
> yacas(" 10#LegendreRecursive(0,_x)<--1  ")
> yacas(" 20#LegendreRecursive(1,_x)<--x  ")
> yacas(" 30#LegendreRecursive(n_IsPositiveInteger,_x)<--Expand(((2*n-1)*x*LegendreRecursive(n-1,x)-(n-1)*LegendreRecursive(n-2,x))/n)  ")
> yacas(" Table(LegendreRecursive(i,x),i,0,4,1)  ")
%
%1
%
%x
%
%          2
%-1 + 3 * x 
%-----------
%     2     
%
% ...
%
%39. Das vierte Legendre Polynom an der Stelle 1 :
%   (OK) Compute Legendre polynomial P[4] at x=1. 
> yacas(" Verify(OrthoP(4,1), 1) ")
> yacas(" OrthoP(4,1) ")
%
> yacas(" Legendre(4,1)  ")
% Out> 1 
%
%40. Definieren des Polynoms p = sum( i=1..5, ai*x^i ) :
%(OK) Define the polynomial p=Sum(i,1,5,a[i]*x^i). 
> yacas(" p:=Sum(i,1,5,a[i]*x^i) ")
> yacas(" Verify(p, a[1]*x+a[2]*x^2+a[3]*x^3  +a[4]*x^4+a[5]*x^5) ")
%
> yacas(" ans:=Add(MakeVector(a,5)*FillList(x,5)^(1 .. 5))  ")
%               2         3         4         5
%a1 * x + a2 * x  + a3 * x  + a4 * x  + a5 * x 
%
%41. Anwenden des Hornerschemas auf das obige Polynom :
%   (OK) Convert the above to Horner's form. 
> yacas(" Verify(Horner(p, x), ((((a[5]*x+a[4])*x  +a[3])*x+a[2])*x+a[1])*x) ")
> yacas(" Horner(p, x) ")
%
> yacas(" ans:=Add(MakeVector(a,5)*FillList(x,5)^(1 ..5))  ")
> yacas(" Horner(ans,x)  ")
%
%( ( ( ( a5 * x + a4 ) * x + a3 ) * x + a2 ) * x + a1 ) * x
%
%42. (NO) Convert the result of problem 127 to Fortran syntax. 
> yacas(" CForm(Horner(p, x)) ")
%
%43. Berechnung der Wahrheitswerte TRUE und FALSE :
%(OK) Verify that True And False=False. 
> yacas(" Verify(True And False, False) ")
%
> yacas(" True And False ")
% Out>  False 
%
%44. (OK) Prove x Or Not x. 
> yacas(" Verify(CanProve(x Or Not x), True) ")
> yacas(" CanProve(x Or Not x) ")
%
%45. (OK) Prove x Or y Or x And y=>x Or y. 
> yacas(" Verify(CanProve(x Or y Or x And y => x Or y)  , True) ")
> yacas(" CanProve(x Or y Or x And y => x Or y) ")
%
%46. L�sung der Gleichung tan(x) = 1 :
%   (BUG) Solve Tan(x)=1 and get all solutions. 
> yacas(" Verify(Solve(Tan(x)==1,x), {x==Pi/4}) ")
%(get only one solution) 
> yacas("  Solve(Tan(x)==1,x)  ")
% Out> Pi/4 
%
%47. Die inverse Taylorentwicklung von sin(y) + cos(y) an der Stelle y=0.
%    Inverse Taylor expansion of sin(y) + cos(y) at position y=0
> yacas(" InverseTaylor(y,0,6)Sin(y)+Cos(y)  ")
%
%                 2                3                              5
%        ( y - 1 )    2 * ( y - 1 )             4   17 * ( y - 1 ) 
%y - 1 + ---------- + -------------- + ( y - 1 )  + ---------------
%            2              3                             10       
%
%
%�berpr�fung, dass es sich bis zur 5. Ordnung wirklich um das Inverse handelt:
%Test, that this is the inverse up to order 5.
> yacas(" s:=Taylor(y,0,6)Sin(y)+Cos(y) ")
> yacas(" BigOh(Subst(y,s)t,y,6)  ")
% Out> y 
%
%48. L�sung des linearen Gleichungssystems
%    x+y+z=6,2x+y+2z=10,x+3y+z=10 :
% (OK) Solve a degenerate 3x3 linear system. 
> yacas(" Verify(OldSolve({x+y+z==6, 2*x+y+2*z==10, x+3*y+z==10}, {x,y,z}), {{4-z,2,z}}) ")
%
> yacas(" OldSolve({x+y+z==6,2*x+y+2*z==10,x+3*y+z==10},{x,y,z})  ")
% Out> {{4-z,2,z}} 
% Einige Beispielberechnungen mit Yacas:
% Some example calculations with Yacas.
%49. Integration von (Sin(n*x)*Cos(m*x)) in den Grenzen -Pi bis +Pi :
%    Integrate                           from               to      .
> yacas(" Simplify(Integrate(x,-Pi,Pi)Sin(x)*Sin(2*x)) ")
% Out> 0 
> yacas(" Simplify(Integrate(x,-Pi,Pi)Sin(2*x)*Sin(2*x))  ")
% Out> Pi 
> yacas("  Simplify(Integrate(x,-Pi,Pi)Sin(5*x)*Sin(5*x))  ")
% Out> Pi 
> yacas("  Simplify(Integrate(x,-Pi,Pi)Cos(x)*Cos(2*x))  ")
% Out> 0 
> yacas(" Simplify(Integrate(x,-Pi,Pi)Cos(2*x)*Cos(2*x))  ")
% Out> Pi 
> yacas("  Simplify(Integrate(x,-Pi,Pi)Cos(5*x)*Cos(5*x))  ")
% Out> Pi 
> yacas("  Simplify(Integrate(x,-Pi,Pi)Sin(x)*Cos(2*x))  ")
% Out> 0 
> yacas(" Simplify(Integrate(x,-Pi,Pi)Sin(2*x)*Cos(2*x))  ")
% Out> 0 
> yacas(" Simplify(Integrate(x,-Pi,Pi)Sin(5*x)*Cos(5*x))  ")
% Out> 0 
%
%50. Die ersten 5 Koeffizienten der Fourierreihe von x^2
%   auf dem Intervall [-Pi | Pi]. :
%   The fist 5 coefficients of Fourier series of x^2 on the Intervall [-Pi | Pi]. 
> yacas(" Fourier(_n,_f)<--1/Pi*Integrate(x,-Pi,Pi)f*Cos(n*x) ")
% Out> True 
> yacas("  Table(Fourier(n,x^2),n,0,5,1)  ")
% (2*Pi^2)/3
%-4
%1
%-4/9
%1/4
%-4/25
%Out> True 
%
%51. Testen, dass f:=x*Exp(-x/2) eine L�sung der Gleichung
%   H(f)=E*f ist, wobei E eine Konstante und H=D(x)D(x)f + f/x ist :
%   Test, if f:=x*Exp(-x/2) is a solution of equation
%   H(f)=E*f , where E is constant and equals H=D(x)D(x)f + f/x:
> yacas(" H(f):=Deriv(x)Deriv(x)f+f/x  ")
> yacas(" f:=x*Exp(-x/2)  ")
> yacas(" res:=H(f)  ")
>> Simplify(res)
%
%       /  / x \ \
%x * Exp| -| - | |
%       \  \ 2 / /
%-----------------
%        4        
%
> yacas(" Simplify(res/f)  ")
%
%1/4
%
%52. Zeigen, dass die ersten Summanden der Taylorentwicklungen
%   von Sin(x) und Cos(x-Pi/2) gleich sind :
%   show, that the first terms of the Taylor expamsion
%   of Sin(x) and Cos(x-Pi/2) are equal:
%
> yacas(" ans1:=Taylor(x,0,8)Sin(x)  ")
%
%     3    5      7 
%    x    x      x  
%x - -- + --- - ----
%    6    120   5040
%
> yacas(" ans2:=Taylor(x,0,8)Cos(x-Pi/2)  ")
%
%     3    5      7 
%    x    x      x  
%x - -- + --- - ----
%    6    120   5040
%
> yacas(" ans1-ans2  ")
% Out> 0 
%
%53. Bestimmung eine Polynoms, dessen Graph durch die Punkte
%    (x,y) = { (-2,4), (1,1), (3,9) } geht und Nachweis, dass
%    es sich dabei um die Funktion x^2 handelt :
%    Finding a polynom, whose graph goes to the points
%    (x,y) = { (-2,4), (1,1), (3,9) } and testing, if
%    it is the function x^2:
%
> yacas(" ans:=LagrangeInterpolant({-2,1,3},{4,1,9},x)  ")
%
%4 * ( x - 1 ) * ( x - 3 )   ( x - -2 ) * ( x - 3 )   9 * ( x - -2 ) * ( x - 1 )
%------------------------- - ---------------------- + --------------------------
%           15                         6                          10            
%
> yacas(" Simplify(ans)  ")
%
% 2
%x 
%
%54. (OK) Evaluate the Bessel function J[2] numerically at z=1+I. 
> yacas(" BesselJ(2, 1+I)    ")
> yacas(" N(BesselJ(2, 1+I)) ")
> yacas(" NumericEqual(N(BesselJ(2, 1+I)),  0.4157988694e-1+I*0.2473976415,GetPrecision()) ")
>
